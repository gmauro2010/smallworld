﻿namespace SmallWorld
{
    partial class FrmGameMenu
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BtnAnimals = new Button();
            SuspendLayout();
            // 
            // BtnAnimals
            // 
            BtnAnimals.FlatStyle = FlatStyle.Flat;
            BtnAnimals.ForeColor = Color.FromArgb(224, 224, 224);
            BtnAnimals.Location = new Point(758, 61);
            BtnAnimals.Name = "BtnAnimals";
            BtnAnimals.Size = new Size(140, 68);
            BtnAnimals.TabIndex = 10;
            BtnAnimals.Text = "Creador de entidades";
            BtnAnimals.UseVisualStyleBackColor = true;
            BtnAnimals.Click += BtnAnimals_Click;
            // 
            // FrmGameMenu
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(64, 64, 64);
            ClientSize = new Size(1067, 654);
            Controls.Add(BtnAnimals);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Name = "FrmGameMenu";
            Text = "Small World Alpha 1.0";
            Load += Form1_Load;
            ResumeLayout(false);
        }

        #endregion
        private Button BtnAnimals;
    }
}