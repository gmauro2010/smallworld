using SmallWorld.Src.controllers;
using SmallWorld.src.views;
using System.Reflection.Metadata;

namespace SmallWorld
{
    public partial class FrmGameMenu : Form
    {
        private EntitiesController entityControl = EntitiesController.GetEntitiesController();

        public FrmGameMenu()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void BtnAnimals_Click(object sender, EventArgs e)
        {
            EntityCrud cRUDView = new();
            cRUDView.ShowDialog();
        }
    }
}