﻿namespace SmallWorld.src.views
{
    partial class EntityCrud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            TxtName = new TextBox();
            label1 = new Label();
            label2 = new Label();
            TxtMaxEnergy = new TextBox();
            label3 = new Label();
            label4 = new Label();
            CbbDiet = new ComboBox();
            BtnSaveEntity = new Button();
            GbAnimalCreation = new GroupBox();
            cbbTerrains = new ComboBox();
            label10 = new Label();
            cbbRangeAttack = new ComboBox();
            txtDefPoints = new TextBox();
            label9 = new Label();
            txtAtkPoints = new TextBox();
            label8 = new Label();
            txtMaxLife = new TextBox();
            label7 = new Label();
            cbbKindom = new ComboBox();
            label6 = new Label();
            cbbEnvironment = new ComboBox();
            label5 = new Label();
            DgvAnimals = new DataGridView();
            btnTerrainTest = new Button();
            GbAnimalCreation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)DgvAnimals).BeginInit();
            SuspendLayout();
            // 
            // TxtName
            // 
            TxtName.BackColor = Color.FromArgb(64, 64, 64);
            TxtName.ForeColor = Color.FromArgb(224, 224, 224);
            TxtName.Location = new Point(82, 22);
            TxtName.Name = "TxtName";
            TxtName.Size = new Size(290, 23);
            TxtName.TabIndex = 0;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(19, 25);
            label1.Name = "label1";
            label1.Size = new Size(57, 15);
            label1.TabIndex = 1;
            label1.Text = "Nombre :";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(27, 177);
            label2.Name = "label2";
            label2.Size = new Size(98, 15);
            label2.TabIndex = 3;
            label2.Text = "Energía máxima :";
            // 
            // TxtMaxEnergy
            // 
            TxtMaxEnergy.BackColor = Color.FromArgb(64, 64, 64);
            TxtMaxEnergy.ForeColor = Color.FromArgb(224, 224, 224);
            TxtMaxEnergy.Location = new Point(131, 174);
            TxtMaxEnergy.Name = "TxtMaxEnergy";
            TxtMaxEnergy.Size = new Size(55, 23);
            TxtMaxEnergy.TabIndex = 2;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(209, 235);
            label3.Name = "label3";
            label3.Size = new Size(102, 15);
            label3.TabIndex = 5;
            label3.Text = "Rango de ataque :";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(42, 83);
            label4.Name = "label4";
            label4.Size = new Size(34, 15);
            label4.TabIndex = 7;
            label4.Text = "Diet :";
            // 
            // CbbDiet
            // 
            CbbDiet.BackColor = Color.FromArgb(64, 64, 64);
            CbbDiet.FlatStyle = FlatStyle.Flat;
            CbbDiet.ForeColor = Color.FromArgb(224, 224, 224);
            CbbDiet.FormattingEnabled = true;
            CbbDiet.Location = new Point(82, 80);
            CbbDiet.Name = "CbbDiet";
            CbbDiet.Size = new Size(290, 23);
            CbbDiet.TabIndex = 8;
            // 
            // BtnSaveEntity
            // 
            BtnSaveEntity.FlatStyle = FlatStyle.Flat;
            BtnSaveEntity.ForeColor = Color.FromArgb(224, 224, 224);
            BtnSaveEntity.Location = new Point(12, 274);
            BtnSaveEntity.Name = "BtnSaveEntity";
            BtnSaveEntity.Size = new Size(86, 26);
            BtnSaveEntity.TabIndex = 9;
            BtnSaveEntity.Text = "Guardar";
            BtnSaveEntity.UseVisualStyleBackColor = true;
            BtnSaveEntity.Click += BtnSaveEntity_Click;
            // 
            // GbAnimalCreation
            // 
            GbAnimalCreation.Controls.Add(cbbTerrains);
            GbAnimalCreation.Controls.Add(label10);
            GbAnimalCreation.Controls.Add(cbbRangeAttack);
            GbAnimalCreation.Controls.Add(txtDefPoints);
            GbAnimalCreation.Controls.Add(label9);
            GbAnimalCreation.Controls.Add(txtAtkPoints);
            GbAnimalCreation.Controls.Add(label8);
            GbAnimalCreation.Controls.Add(txtMaxLife);
            GbAnimalCreation.Controls.Add(label7);
            GbAnimalCreation.Controls.Add(cbbKindom);
            GbAnimalCreation.Controls.Add(label6);
            GbAnimalCreation.Controls.Add(cbbEnvironment);
            GbAnimalCreation.Controls.Add(label5);
            GbAnimalCreation.Controls.Add(CbbDiet);
            GbAnimalCreation.Controls.Add(TxtName);
            GbAnimalCreation.Controls.Add(label1);
            GbAnimalCreation.Controls.Add(label4);
            GbAnimalCreation.Controls.Add(TxtMaxEnergy);
            GbAnimalCreation.Controls.Add(label3);
            GbAnimalCreation.Controls.Add(label2);
            GbAnimalCreation.ForeColor = Color.FromArgb(224, 224, 224);
            GbAnimalCreation.Location = new Point(12, 4);
            GbAnimalCreation.Name = "GbAnimalCreation";
            GbAnimalCreation.Size = new Size(378, 264);
            GbAnimalCreation.TabIndex = 10;
            GbAnimalCreation.TabStop = false;
            GbAnimalCreation.Text = "Criatura";
            // 
            // cbbTerrains
            // 
            cbbTerrains.BackColor = Color.FromArgb(64, 64, 64);
            cbbTerrains.FlatStyle = FlatStyle.Flat;
            cbbTerrains.ForeColor = Color.FromArgb(224, 224, 224);
            cbbTerrains.FormattingEnabled = true;
            cbbTerrains.Location = new Point(82, 138);
            cbbTerrains.Name = "cbbTerrains";
            cbbTerrains.Size = new Size(290, 23);
            cbbTerrains.TabIndex = 22;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new Point(11, 141);
            label10.Name = "label10";
            label10.Size = new Size(53, 15);
            label10.TabIndex = 21;
            label10.Text = "Terrains :";
            // 
            // cbbRangeAttack
            // 
            cbbRangeAttack.BackColor = Color.FromArgb(64, 64, 64);
            cbbRangeAttack.FlatStyle = FlatStyle.Flat;
            cbbRangeAttack.ForeColor = Color.FromArgb(224, 224, 224);
            cbbRangeAttack.FormattingEnabled = true;
            cbbRangeAttack.Items.AddRange(new object[] { "0", "1" });
            cbbRangeAttack.Location = new Point(317, 232);
            cbbRangeAttack.Name = "cbbRangeAttack";
            cbbRangeAttack.Size = new Size(55, 23);
            cbbRangeAttack.TabIndex = 20;
            // 
            // txtDefPoints
            // 
            txtDefPoints.BackColor = Color.FromArgb(64, 64, 64);
            txtDefPoints.ForeColor = Color.FromArgb(224, 224, 224);
            txtDefPoints.Location = new Point(317, 203);
            txtDefPoints.Name = "txtDefPoints";
            txtDefPoints.Size = new Size(55, 23);
            txtDefPoints.TabIndex = 18;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(201, 206);
            label9.Name = "label9";
            label9.Size = new Size(110, 15);
            label9.TabIndex = 19;
            label9.Text = "Puntos de defensa :";
            // 
            // txtAtkPoints
            // 
            txtAtkPoints.BackColor = Color.FromArgb(64, 64, 64);
            txtAtkPoints.ForeColor = Color.FromArgb(224, 224, 224);
            txtAtkPoints.Location = new Point(317, 174);
            txtAtkPoints.Name = "txtAtkPoints";
            txtAtkPoints.Size = new Size(55, 23);
            txtAtkPoints.TabIndex = 16;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(206, 177);
            label8.Name = "label8";
            label8.Size = new Size(105, 15);
            label8.TabIndex = 17;
            label8.Text = "Puntos de ataque :";
            // 
            // txtMaxLife
            // 
            txtMaxLife.BackColor = Color.FromArgb(64, 64, 64);
            txtMaxLife.ForeColor = Color.FromArgb(224, 224, 224);
            txtMaxLife.Location = new Point(131, 203);
            txtMaxLife.Name = "txtMaxLife";
            txtMaxLife.Size = new Size(55, 23);
            txtMaxLife.TabIndex = 14;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(43, 206);
            label7.Name = "label7";
            label7.Size = new Size(82, 15);
            label7.TabIndex = 15;
            label7.Text = "Vida máxima :";
            // 
            // cbbKindom
            // 
            cbbKindom.BackColor = Color.FromArgb(64, 64, 64);
            cbbKindom.FlatStyle = FlatStyle.Flat;
            cbbKindom.ForeColor = Color.FromArgb(224, 224, 224);
            cbbKindom.FormattingEnabled = true;
            cbbKindom.Location = new Point(82, 51);
            cbbKindom.Name = "cbbKindom";
            cbbKindom.Size = new Size(290, 23);
            cbbKindom.TabIndex = 13;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(33, 54);
            label6.Name = "label6";
            label6.Size = new Size(43, 15);
            label6.TabIndex = 12;
            label6.Text = "Reino :";
            // 
            // cbbEnvironment
            // 
            cbbEnvironment.BackColor = Color.FromArgb(64, 64, 64);
            cbbEnvironment.FlatStyle = FlatStyle.Flat;
            cbbEnvironment.ForeColor = Color.FromArgb(224, 224, 224);
            cbbEnvironment.FormattingEnabled = true;
            cbbEnvironment.Location = new Point(82, 109);
            cbbEnvironment.Name = "cbbEnvironment";
            cbbEnvironment.Size = new Size(290, 23);
            cbbEnvironment.TabIndex = 11;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(11, 112);
            label5.Name = "label5";
            label5.Size = new Size(65, 15);
            label5.TabIndex = 10;
            label5.Text = "Ambiente :";
            // 
            // DgvAnimals
            // 
            DgvAnimals.BorderStyle = BorderStyle.None;
            DgvAnimals.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            DgvAnimals.Location = new Point(396, 12);
            DgvAnimals.Name = "DgvAnimals";
            DgvAnimals.RowTemplate.Height = 25;
            DgvAnimals.Size = new Size(494, 363);
            DgvAnimals.TabIndex = 11;
            // 
            // btnTerrainTest
            // 
            btnTerrainTest.FlatStyle = FlatStyle.Flat;
            btnTerrainTest.ForeColor = Color.FromArgb(224, 224, 224);
            btnTerrainTest.Location = new Point(158, 274);
            btnTerrainTest.Name = "btnTerrainTest";
            btnTerrainTest.Size = new Size(195, 26);
            btnTerrainTest.TabIndex = 12;
            btnTerrainTest.Text = "TestMoveOnSelectedTerrain";
            btnTerrainTest.UseVisualStyleBackColor = true;
            btnTerrainTest.Click += btnTerrainTest_Click;
            // 
            // EntityCrud
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(64, 64, 64);
            ClientSize = new Size(902, 387);
            Controls.Add(btnTerrainTest);
            Controls.Add(DgvAnimals);
            Controls.Add(GbAnimalCreation);
            Controls.Add(BtnSaveEntity);
            ForeColor = Color.FromArgb(224, 224, 224);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Name = "EntityCrud";
            Text = "CRUDView";
            GbAnimalCreation.ResumeLayout(false);
            GbAnimalCreation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)DgvAnimals).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private TextBox TxtName;
        private Label label1;
        private Label label2;
        private TextBox TxtMaxEnergy;
        private Label label3;
        private Label label4;
        private ComboBox CbbDiet;
        private Button BtnSaveEntity;
        private GroupBox GbAnimalCreation;
        private DataGridView DgvAnimals;
        private ComboBox cbbKindom;
        private Label label6;
        private ComboBox cbbEnvironment;
        private Label label5;
        private TextBox txtAtkPoints;
        private Label label8;
        private TextBox txtMaxLife;
        private Label label7;
        private TextBox txtDefPoints;
        private Label label9;
        private ComboBox cbbRangeAttack;
        private ComboBox cbbTerrains;
        private Label label10;
        private Button btnTerrainTest;
    }
}