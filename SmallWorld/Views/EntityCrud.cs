﻿using SmallWorld.Src.controllers;
using SmallWorld.Src.model.dietType;
using SmallWorld.Src.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmallWorld.Src.Model.Kingdoms;
using SmallWorld.Src.Model.Diets;
using SmallWorld.Src.Model.Terrains;
using SmallWorld.Src.Controllers;

namespace SmallWorld.src.views
{
    public partial class EntityCrud : Form
    {
        private readonly EntitiesController EntitiesMannager = EntitiesController.GetEntitiesController();
        private readonly EnvironmentController EnvironmentsMannager = EnvironmentController.GetEnvironmentController();

        public EntityCrud()
        {
            InitializeComponent();

            //Se carga comboBox con los reinos disponibles.
            cbbKindom.Items.Add(new Animal());
            cbbKindom.Items.Add(new Vegetable());
            cbbKindom.Items.Add(new Machine());
            cbbKindom.SelectedIndex = 0;

            //Se carga comboBox con las dietas disponibles.
            CbbDiet.Items.Add(new Carnivore());
            CbbDiet.Items.Add(new HervivorousDiet());
            CbbDiet.Items.Add(new Omnivorous());
            CbbDiet.SelectedIndex = 0;

            //Se carga comboBox con los ambientes disponibles.
            foreach (IEnvironment environment in EnvironmentsMannager.getEnvironmentsCombinations())
                cbbEnvironment.Items.Add(environment);
            cbbEnvironment.SelectedIndex = 0;

                //Se carga comboBox para testear Decorator Pattern
            cbbTerrains.Items.Add(new Land());
            cbbTerrains.Items.Add(new Water());
            cbbTerrains.Items.Add(new Air());
            cbbTerrains.SelectedIndex = 0;
        }

        private void BtnSaveEntity_Click(object sender, EventArgs e)
        {
            List<IEnvironment> selectedEnvironmentList = new List<IEnvironment>();

            foreach (IEnvironment environment in cbbEnvironment.Items)
            {
                selectedEnvironmentList.Add(environment);
            }

            EntitiesMannager.AddEntity(TxtName.Text, (IKingdom)cbbKindom.SelectedItem, selectedEnvironmentList, (IDiet)CbbDiet.SelectedItem,
                Convert.ToInt32(TxtMaxEnergy.Text), Convert.ToInt32(txtMaxLife.Text), Convert.ToInt32(txtAtkPoints.Text), Convert.ToInt32(txtDefPoints.Text),
                Convert.ToInt32(cbbRangeAttack.SelectedItem));
        }

        private void AddEntityToDataGridView()
        {
            DgvAnimals.DataSource = EntitiesMannager.ENTITIES;
        }

        private void btnTerrainTest_Click(object sender, EventArgs e)
        {
            if (((IEnvironment)cbbEnvironment.SelectedItem).CanMoveThrough((ITerrain)cbbTerrains.SelectedItem))
                MessageBox.Show("Puede");
            else
                MessageBox.Show("No puede");
        }
    }
}
