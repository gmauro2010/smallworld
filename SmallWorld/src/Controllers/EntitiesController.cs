﻿using SmallWorld.Src.Interfaces;
using SmallWorld.Src.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.controllers
{
    internal class EntitiesController // Controladora que maneja las entidades disponibles del juego.
    {
        // ATRIBUTOS
        private static EntitiesController? controllerInstance; // Campo estático para implementar el patrón Singleton.       
        private List<Entity> Entities = new(); // Lista de todas las entidades del juego.
        
        // CONSTRUCTORES
        private EntitiesController() { }//Constructor privado para implementar el patrón Singleton.
        
        // PROPIEDADES
        public List<Entity> ENTITIES // Propiedad que retorna la lista de entidades.
        {
            get { return Entities; }
        }

        // MÉTODOS
        public static EntitiesController GetEntitiesController()//Método que retorna la única instancia de la controladora.
        {
            if (controllerInstance == null)
                controllerInstance = new EntitiesController();
            return controllerInstance;
        }
        public void AddEntity(string name,IKingdom kingdom,List<IEnvironment> environments,
            IDiet diet, int maxEnergy, int maxLife, int atkPoints, int DefPoints, int atkRange) //Método que agrega una entidad a la lista de entidades.
        {
            Entities.Add(new Entity(name, kingdom, environments,
             diet, maxEnergy, maxLife, atkPoints, DefPoints, atkRange));
        }        
        public void DeleteEntity(Entity entity) // Método que elimina una entidad de la lista
        {
            ENTITIES.Remove(entity);
        }

    }
}
