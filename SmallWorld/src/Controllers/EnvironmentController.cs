﻿using SmallWorld.Src.controllers;
using SmallWorld.Src.Interfaces;
using SmallWorld.Src.model.animalsType;
using SmallWorld.Src.Model;
using SmallWorld.Src.Model.Environments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Controllers
{
    internal class EnvironmentController
    {
        // ATRIBUTOS
        private static EnvironmentController? controllerInstance; // Campo estático para implementar el patrón Singleton.       
        private List<IEnvironment> Environments = new(); // Lista de ambientes.

        // CONSTRUCTORES
        private EnvironmentController() { }//Constructor privado para implementar el patrón Singleton.

        // PROPIEDADES
        public List<IEnvironment> ENVIRONMENTS // Propiedad que retorna la lista de ambientes.
        {
            get { return Environments; }
        }
        //MÉTODOS
        public static EnvironmentController GetEnvironmentController()//Método que retorna la única instancia de la controladora.
        {
            if (controllerInstance == null)
            {
                controllerInstance = new EnvironmentController();
            }                
             
            return controllerInstance;
        }

        public List<IEnvironment> getEnvironmentsCombinations() 
        {
            List<IEnvironment> environmentsList = new List<IEnvironment>();
            environmentsList.Add(getAquaticEnvironment());
            environmentsList.Add(getAerealEnvironment());
            environmentsList.Add(getTerrestialEnvironment());

            environmentsList.Add(getAquaticAerealEnvironment());
            environmentsList.Add(getAquaticTerrestialEnvironment());

            environmentsList.Add(getAerealAquaticTerrestialEnvironment());

            return environmentsList;
            
        }
        public IEnvironment getAquaticEnvironment()
        {
            EnvironmentComponent aquaticComponent = new EnvironmentComponent();
            return new AquaticDecorator(aquaticComponent);
        }
        public IEnvironment getAerealEnvironment()
        {
            EnvironmentComponent aerealComponent = new EnvironmentComponent();
            return new AerealDecorator(aerealComponent);
        }
        public IEnvironment getTerrestialEnvironment()
        {
            EnvironmentComponent terrestialComponent = new EnvironmentComponent();
            return new TerrestialDecorator(terrestialComponent);
        }
        public IEnvironment getAquaticAerealEnvironment()
        {
            EnvironmentComponent aquaticAerealComponent = new EnvironmentComponent();
            AquaticDecorator aquaticDecorator = new AquaticDecorator(aquaticAerealComponent);
            return new AerealDecorator(aquaticDecorator);
        }
        public IEnvironment getAquaticTerrestialEnvironment()
        {
            EnvironmentComponent aquaticTerrestialComponent = new EnvironmentComponent();
            AquaticDecorator aquaticTerrestialDecorator = new AquaticDecorator(aquaticTerrestialComponent);
            return new TerrestialDecorator(aquaticTerrestialDecorator);
        }
        public IEnvironment getAerealAquaticTerrestialEnvironment()
        {
            EnvironmentComponent aerealTerrestialComponent = new EnvironmentComponent();
            AerealDecorator aerealDecorator = new AerealDecorator(aerealTerrestialComponent);
            AquaticDecorator aquaticDecorator = new AquaticDecorator(aerealDecorator);
            return new TerrestialDecorator(aquaticDecorator);
        }       

    }
}
