﻿using SmallWorld.Src.Interfaces;
using SmallWorld.Src.Model.Kingdoms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.model.dietType
{
    internal class HervivorousDiet : IDiet
    {
        public bool CanEat(IFood food)
        {
            return food is Vegetable;
        }
        public override string ToString()
        {
            return "Hervívoro";
        }
    }
}
