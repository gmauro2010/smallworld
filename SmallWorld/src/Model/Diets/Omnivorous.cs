﻿using SmallWorld.Src.Interfaces;
using SmallWorld.Src.Model.Kingdoms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.model.dietType
{
    internal class Omnivorous : IDiet
    {
        public bool CanEat(IFood food)
        {
            return food is Vegetable || food is Animal;
        }
        public override string ToString()
        {
            return "Omnívora";
        }
    }
}
