﻿using SmallWorld.Src.Interfaces;
using SmallWorld.Src.Model.Interactables.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model
{
    internal class Entity : IInteractable,IPositionable
    {
        //Atributos

        string? Name;
        IKingdom? Kingdom;        
        List<IEnvironment?> Environments;
        IDiet? Diet;
        int MaxEnergy = 0;
        int CurrentEnergy = 0;
        int MaxLife = 0;
        int CurrentLife = 0;
        int AtkPoints = 0;
        int DefPoints = 0;
        int AtkRange = 0;


        //Constructores
        public Entity(string name, IKingdom kingdom, List<IEnvironment> environments,
            IDiet diet, int maxEnergy, int maxLife, int atkPoints, int DefPoints, int atkRange)
        {
            NAME = name;
            KINGDOM = kingdom;
            ENVIRONMENTS = environments;
            DIET = diet;
            MAX_ENERGY = maxEnergy;
            CURRENT_ENERGY = maxEnergy;
            MAX_LIFE = maxLife;
            CURRENT_LIFE = maxLife;
            ATK_POINTS = atkPoints;
            DEF_POINTS = atkRange;
        }

        //Propiedades        
        public string NAME
        {
            set { if(value!=null) Name = value; }
            get => Name;
        }
        public IKingdom KINGDOM
        {
            set { if (value != null) Kingdom = value; }
            get => Kingdom;
        }
        public List<IEnvironment> ENVIRONMENTS
        {
            set { if (value != null) Environments = value; }
            get => Environments;
        }
        public IDiet DIET
        {
            set { Diet = value; }
            get => Diet;
        }  
        public int CURRENT_ENERGY
        {
            set { if (value > 0) CurrentEnergy = value; }
            get => CurrentEnergy;
        }
        public int MAX_ENERGY
        {
            set { if (value > 0) MaxEnergy = value; }
            get => MaxEnergy;
        }
        public int CURRENT_LIFE
        {
            set { if (value > 0) CurrentLife = value; }
            get => CurrentLife;
        }
        public int MAX_LIFE
        {
            set { if (value > 0) MaxLife = value; }
            get => MaxLife;
        }
        public int ATK_POINTS
        {
            set { if (value > 0) AtkPoints = value; }
            get => AtkPoints;
        }
        public int DEF_POINTS
        {
            set { if (value > 0) DefPoints = value; }
            get => DefPoints;
        }
        public int ATK_RANGE
        {
            set { if (value > 0) AtkRange = value; }
            get => AtkRange;
        }

        //Métodos
        public bool MoveThrough(ITerrain terrain)
        {
            foreach (IEnvironment environment in ENVIRONMENTS)
                if (environment.CanMoveThrough(terrain))
                    return true;
            return false;
        }
        public List<IEnvironment> GetEnvironment()
        {
            return ENVIRONMENTS;
        }
        public IDiet GetDiet()
        {
            return DIET;
        }
        public bool Eat(IFood food)
        {
            bool edible = DIET.CanEat(food);
            if (edible)
                CURRENT_ENERGY += food.GetCalories();
            return edible;

            /* Otra implementación

              if (Diet.CanEat(food))
                  _Energy += food.GetCalories();
              return Diet.CanEat(food);
            */

        }
        public int Rest()
        {
            return CURRENT_ENERGY += MAX_ENERGY / 2;
        }
        public void Attack(Entity entity)
        {
        }
        public void Die()
        {
        }
        private void BeingAttacked(int atkPoints)
        {
        }
        public void UseItem(Item item)
        {
            item.Interact(this);
        }
        public void Interact(Entity entity)
        {
            BeingAttacked(entity.AtkPoints);
        }

        public bool PlaceOnTerrain(ITerrain terrain)
        {
            return false;
        }

        public bool Move(ITerrain terrainOrigin, ITerrain terrainDestiny)
        {
            throw new NotImplementedException();
        }

        public bool RemoveFromTerrain(ITerrain terrain)
        {
            throw new NotImplementedException();
        }
    }
}
