﻿using SmallWorld.Src.Interfaces;
using SmallWorld.Src.Model.Terrains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Environments
{
    internal class AquaticDecorator : EnvironmentBaseDecorator
    {
        public AquaticDecorator(IEnvironment wrappee) : base(wrappee)
        {            
        }

        public override bool CanMoveThrough(ITerrain terrain)
        {
            return base.CanMoveThrough(terrain) || terrain is Water;
        }

        public override string ToString()
        {
            return base.ToString() + " Aquatic ";
        }
    }
}
