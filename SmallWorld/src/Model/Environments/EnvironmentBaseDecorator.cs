﻿using SmallWorld.Src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Environments
{
    internal class EnvironmentBaseDecorator : IEnvironment
    {
        IEnvironment EnvironmentNotifier;

        public EnvironmentBaseDecorator(IEnvironment environment)
        {
            EnvironmentNotifier = environment;
        }
        public virtual bool CanMoveThrough(ITerrain terrain)
        {
            return EnvironmentNotifier.CanMoveThrough(terrain);
        }

        public override string ToString()
        {
            return EnvironmentNotifier.ToString();
        }
    }
}
