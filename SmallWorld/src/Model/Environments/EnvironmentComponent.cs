﻿using SmallWorld.Src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Environments
{
    internal class EnvironmentComponent : IEnvironment
    {
        public virtual bool CanMoveThrough(ITerrain terrain)
        {
            return false;
        }
        public override string ToString()
        {
            return "";
        }
    }
}
