﻿using SmallWorld.Src.Interfaces;
using SmallWorld.Src.Model.Terrains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallWorld.Src.Model.Environments;

namespace SmallWorld.Src.model.animalsType
{
    internal class TerrestialDecorator : EnvironmentBaseDecorator
    {
        public TerrestialDecorator(IEnvironment wrappee) : base(wrappee)
        {
        }

        public override bool CanMoveThrough(ITerrain terrain)
        {
            return base.CanMoveThrough(terrain) || terrain is Land;
        }

        public override string ToString()
        {
            return base.ToString() + " Terrestial ";
        }
    }
}
