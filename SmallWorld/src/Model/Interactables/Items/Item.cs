﻿using SmallWorld.Src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmallWorld.Src.Model.Interactables.Items.Strategies;

namespace SmallWorld.Src.Model.Interactables.Items
{
    internal class Item : IInteractable
    {
        IItemStrategy _strategy = new IncreaseEnergyPoints();
        int _value = 0;

        public void Interact(Entity entity)
        {
            _strategy.ApplyEffect(entity);
        }

        public IItemStrategy Strategy
        {
            set => _strategy = value;
            get => _strategy;
        }

        public int Value
        {
            set => _value = value;
            get => _value;
        }
    }
}
