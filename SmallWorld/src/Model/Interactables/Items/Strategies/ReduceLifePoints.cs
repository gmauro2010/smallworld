﻿using SmallWorld.Src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Interactables.Items.Strategies
{
    internal class ReduceLifePoints: IItemStrategy
    {
        public void ApplyEffect(Entity entity)
        {
            entity.CURRENT_LIFE -= 100;
        }
    }
}
