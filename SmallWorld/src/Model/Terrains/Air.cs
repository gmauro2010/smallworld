﻿using SmallWorld.Src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Terrains
{
    internal class Air : ITerrain
    {
        List<IPositionable> positionables = new List<IPositionable>();

        public void placePositionable(IPositionable positionable)
        {
            positionables.Add(positionable);
        }
        public void RemovePositionable(IPositionable positionable)
        {
            positionables.Remove(positionable);
        }

        public override string ToString()
        {   
            return "Aire";
        }
    }
}
