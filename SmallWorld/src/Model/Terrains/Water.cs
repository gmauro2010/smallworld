﻿using SmallWorld.Src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Terrains
{
    internal class Water : ITerrain
    {
        List<IPositionable> positionables = new List<IPositionable>();
        public override string ToString()
        {
            return "Agua";
        }
    }
}
