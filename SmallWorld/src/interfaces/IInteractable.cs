﻿using SmallWorld.Src.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Interfaces
{
    internal interface IInteractable
    {
        void Interact(Entity entity);
    }
}
