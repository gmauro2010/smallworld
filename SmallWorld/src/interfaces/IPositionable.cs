﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Interfaces
{
    internal interface IPositionable
    {
        public bool PlaceOnTerrain(ITerrain terrain);
        public bool RemoveFromTerrain(ITerrain terrain);
        public bool Move(ITerrain terrainOrigin, ITerrain terrainDestiny);
    }
}
